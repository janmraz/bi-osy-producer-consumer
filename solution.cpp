#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */

class conditionVariableQueue
{
    std::queue<uint32_t> receivedIds;
    std::mutex m;
    std::condition_variable cond_var;
    bool notified = false;
public:
    void push(uint32_t i)
    {
        std::unique_lock<std::mutex> lock(m);
        receivedIds.push(i);
        notified = true;
        cond_var.notify_one();
    }

    template<typename Consumer>
    void consume(Consumer consumer)
    {
        std::unique_lock<std::mutex> lock(m);
        while (!notified) {
            cond_var.wait(lock);
        }
        while (!receivedIds.empty()) {
            consumer(receivedIds.front());
            receivedIds.pop();
        }
        notified = false;
    }
};

conditionVariableQueue receiveQueue;
conditionVariableQueue transmitQueue;

class CSentinelHacker
{
public:
    static bool              SeqSolve                      ( const vector<uint64_t> & fragments,
                                                             CBigInt         & res );
    void                     AddTransmitter                ( ATransmitter      x );
    void                     AddReceiver                   ( AReceiver         x );
    void                     AddFragment                   ( uint64_t          x );
    void                     Start                         ( unsigned          thrCount );
    void                     Stop                          ( void );
    static uint32_t extractId(uint64_t x);

private:
    void receive(const AReceiver & x);
    void work();
    void transmit(const ATransmitter & x);

    vector<ATransmitter> transmitters;
    vector<AReceiver> receivers;
    vector<thread> receiveThreads;
    vector<thread> workerThreads;
    vector<thread> transmitterThreads;

    std::map<uint32_t, vector<uint64_t>> store;
    std::map<uint32_t, CBigInt> resultStore;

    std::mutex mtxStore;
    std::mutex mtxResultStore;
};

bool CSentinelHacker::SeqSolve(const vector <uint64_t> &fragments, CBigInt &res) {
    bool isOK = true;
    FindPermutations(  &fragments[0], fragments.size(),[&res,&isOK]( const uint8_t * p, size_t t) {
        if(t <= 32){
            isOK = false;
            return;
        }
        res = CountExpressions(&p[4],t-32);
    });
    return isOK;
}

void CSentinelHacker::AddTransmitter(ATransmitter x) {
    this->transmitters.push_back(x);
}
void CSentinelHacker::AddReceiver(AReceiver x) {
    this->receivers.push_back(x);
}
void CSentinelHacker::AddFragment(uint64_t x) {
    uint32_t id = extractId(x);
    printf("data %d \n", id);
    auto linkedListNode = store.find(id);
    if(linkedListNode == store.end()){
        std::unique_lock<std::mutex> lock(mtxStore);
        vector<uint64_t> list = { x };
        store.insert({id, list});
    } else {
        std::unique_lock<std::mutex> lock(mtxStore);
        linkedListNode->second.emplace_back(x);
    }
    receiveQueue.push(id);
}
void CSentinelHacker::Start(unsigned thrCount) {
    printf("start called\n");
    for(const auto & receiver : this->receivers){
        printf("starting receiver thread\n");
        this->receiveThreads.emplace_back(&CSentinelHacker::receive, receiver );
    }
    std::this_thread::sleep_for(std::chrono::seconds(3));
    for(unsigned int i = 0; i < thrCount; i++){
        printf("starting worker thread\n");
        this->workerThreads.emplace_back(&CSentinelHacker::work);
    }
    std::this_thread::sleep_for(std::chrono::seconds(3));
    for(const auto & transmitter : this->transmitters){
        printf("starting transmitter thread\n");
        this->transmitterThreads.emplace_back(&CSentinelHacker::transmit , transmitter );
    }
}
void CSentinelHacker::Stop() {
    cout << "stop called \n";
    for(auto & thread : this->receiveThreads){
        thread.join();
    }
    cout << "waited for receive threads\n";
    for(auto & thread : this->workerThreads){
        thread.join();
    }
    cout << "waited for worker threads\n";
    for(auto & thread : this->transmitterThreads){
        thread.join();
    }
    cout << "waited for transmitted threads\n";
}
void CSentinelHacker::receive(const AReceiver &  x) {
    uint64_t data;
    uint64_t mask = 268435455;
    while(true){
        if(!x->Recv(data)) break;
        CSentinelHacker::AddFragment(data);
    }
}
void CSentinelHacker::work() {
    receiveQueue.consume([this](uint32_t input){
        printf("consuming and sending %d\n", input);
        auto vectorWithData = store.find(input);
        CBigInt result;
        SeqSolve(vectorWithData->second, result);
        resultStore.insert({input, result});
        std::unique_lock<std::mutex> lock(mtxResultStore);
        transmitQueue.push(input);
    });
}
void CSentinelHacker::transmit(const ATransmitter &x) {
    transmitQueue.consume([x, this](uint32_t input){
        printf("consuming %d\n", input);
        auto pairIdAndCRes = resultStore.find(input);
        x->Send(input, pairIdAndCRes->second);
    });
}

uint32_t CSentinelHacker::extractId(uint64_t x) {
    uint64_t mask = 268435455;
    return (uint32_t)(x & mask);
}

#ifndef __PROGTEST__
int                main                                    ( void )
{
    using namespace std::placeholders;
    for ( const auto & x : g_TestSets )
    {
        CBigInt res;
        assert(CSentinelHacker::SeqSolve ( x . m_Fragments, res ));
        assert(CBigInt ( x . m_Result ) . CompareTo ( res ) == 0);
    }

    CSentinelHacker test;
    auto            trans = make_shared<CExampleTransmitter> ();
    AReceiver       recv  = make_shared<CExampleReceiver> ( initializer_list<uint64_t> { 0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755 } );
    AReceiver       recv2  = make_shared<CExampleReceiver> ( initializer_list<uint64_t> { 0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755 } );
    AReceiver       recv3  = make_shared<CExampleReceiver> ( initializer_list<uint64_t> { 0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755 } );

    test . AddTransmitter ( trans );
    test . AddReceiver ( recv );
    test . AddReceiver ( recv2 );
    test . AddReceiver ( recv3 );
    test . Start ( 3 );

    static initializer_list<uint64_t> t1Data = { 0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9, 0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e };
    thread t1 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t1Data );

    static initializer_list<uint64_t> t2Data = { 0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb };
    thread t2 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t2Data );
    FragmentSender ( bind ( &CSentinelHacker::AddFragment, &test, _1 ), initializer_list<uint64_t> { 0x017f4cb42a68, 0x02260000000d, 0x072500000025 } );
    t1 . join ();
    t2 . join ();
    test . Stop ();
    assert ( trans -> TotalSent () == 4 );
    assert ( trans -> TotalIncomplete () == 2 );
    return 0;
}
#endif /* __PROGTEST__ */
